import friendlyfunctions

######################################
## Each python file is itself a module
######################################

# Functions and other objects within the module are available like so:
friendlyfunctions.greetings('Jeremy')

#However, these functions must be accessed through the imported namespace.
greetings('Jeremy')

#We can import the functions directly like so:
from friendlyfunctions import greetings
greetings('Jeremy')

#Often times the modules are imported with an abbreviated name.
import friendlyfunctions as ff
ff.greetings('Jeremy')

######################################
## Modules as scripts
######################################

# When you load a module, it runs whatever is inside like a script.
# Typically, you don't want to depend on this behavior, but you
# should know what's happening.


# This should print '11'
import module_with_scripting

# If you import again, the script will not run again. This is to prevent
# Python from doing too much work. For example, we import numpy in almost every file.
# It's best not to run the numpy scripts every time. If it's imported once, Python
# takes note and makes sure not to run it again.
import module_with_scripting

# We can force Python to rerun the import with importlib. This will print '11' again.
# Again, in production we should never use this. During development, consider
# using IPython's 'run' magic: 
#     
#     %run module_with_scripting.py
#
import importlib
importlib.reload(module_with_scripting)

######################################
## Folders as modules
######################################

# A folder becomes a Python module as soon as you put a file in the folder
# called `__init__.py`. This file can import other modules so that you can organize
# your code into multiple directories.

import foldermodule

# Look at the foldermodule module and guess which of these expression will run
# correctly.

foldermodule.add_one(5)

foldermodule.add_two(5)

foldermodule.greetings('Jeremy')

foldermodule.friendlyfunctions.greetings('Jeremy')


# Why can't I import 'just_a_folder'
import just_a_folder


#########################################################
## Making your package available globally (installing it)
#########################################################

# To do this, you need to create a setup.py file. Then, once it's created and has the appropriate
# ingredients, you can install it using `pip`. We'll learn more about this later.
